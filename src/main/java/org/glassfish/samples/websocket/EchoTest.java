
package org.glassfish.samples.websocket;

import java.io.IOException;
import javax.websocket.OnOpen;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;



@ServerEndpoint("/echo2")
public class EchoTest {

    private Session session;

    @OnOpen
    public void onOpen(Session session){
        this.session = session;
    }
    
    @OnMessage
    public String echo2(String message) throws IOException {
      
       
                    session.getBasicRemote().sendText("message form onOpen of server");
        return "Chaoooo";
    }
    
    
}
