package org.glassfish.samples.websocket;

import Navixy.NavixyAPICalls;
import static Navixy.NavixyAPICalls.navixyGetTrackers;
import static Navixy.NavixyAPICalls.navixyTrackerGetState;
import static Navixy.NavixyAPICalls.getNavixyTrackerState;
import static Navixy.NavixyAPICalls.navixyElectronicLockOpenCommand2;
import static Navixy.NavixyAPICalls.printHashMap;
import static Navixy.NavixyAPICalls.*;

import Navixy.NavixyTrackerState;
import Navixy.NavixyUserInfo;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/echo")
public class EchoBavaro {

    /**
     * Incoming message is represented as parameter and return value is going to
     * be send back to peer.
     * </p> {@link javax.websocket.Session} can be put as a parameter and then
     * you can gain better control of whole communication, like sending more
     * than one message, process path parameters,
     * {@link javax.websocket.Extension Extensions} and sub-protocols and much
     * more.
     *
     * @param message incoming text message.
     * @return outgoing message.
     *
     * @see OnMessage
     * @see javax.websocket.Session
     */
    private Session session;

    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
    }

    @OnMessage
    public String echo(String message) throws IOException, InterruptedException {

        Gson gson = new Gson();
        Boolean isSuccess;
        List<String> params = Arrays.asList(message.split(","));  // Recibimos 3 parametros separados por coma, y aqui los dividimos

        String userHash = params.get(0);
        String[] trackersID = params.get(1).split(":");
        String method = params.get(2);

        Map<String, Object> navixyResponseMap = null;

        navixyResponseMap = navixyGetUserInfo(userHash);

        isSuccess = (Boolean) navixyResponseMap.get("success");

        if (!isSuccess) {

            System.out.println("ERROR EN HASH");
            printHashMap(navixyResponseMap);

            message = gson.toJson(navixyResponseMap);

            return message;
        }

        Map<String, Object> mapNavixyTrackerState;
        Object objTrackerState;
        int segundos = 1;
        String stateValue;
        NavixyTrackerState navixyTrackerState;

        System.out.println("^^^^^^^^^^^^^^^^ Entro Bucle 1");

//----      
        if (method.equals("LIVE")) {

            int counter = 30;

            Map<String, Object> camionesTreeMap;
            camionesTreeMap = navixyGetTrackers(userHash);

            do {
                // hay formas mas faciles de hacer esto, pero esta garantiza que los objetos POJO fueron creados a partir de los servicios de Navixy
                ArrayList<Object> respuesta = new ArrayList<Object>();
                String label="";
                for (int i = 0; i < trackersID.length; i++) {
                    mapNavixyTrackerState = navixyTrackerGetState(userHash, trackersID[i]); // obtengo el mapa de respuesta del servicio
                    for (int j = 0; j < camionesTreeMap.size(); j++) {
                        ArrayList<Object> fff =  (ArrayList<Object>) camionesTreeMap.get("list");
                        LinkedTreeMap ffff = (LinkedTreeMap)fff.get(j);
                        if(utilDoubleIntStr(((LinkedTreeMap)fff.get(j)).get("id")).equals(trackersID[i])){
                        label = (String) ffff.get("label");
                        break;
                        //label = (String) ((ArrayList)(camionesTreeMap.get("list").get(j)))  .get("label");
                        }
                    }
                    //session.getBasicRemote().sendText("PRUEBA >>  "+gson.toJson(mapNavixyTrackerState));

                    boolean resultado = (boolean) mapNavixyTrackerState.get("success");
                    if (resultado) {
                        Map<String, Object> listState = new HashMap<String, Object>();
                        Map<String, Object> listStateMap = (Map<String, Object>) mapNavixyTrackerState.get("state");      // obtengo el mapa con el state                                        
                        listState = (Map<String, Object>) listStateMap.get("gps");
                        listState.put("success", mapNavixyTrackerState.get("success"));
                        listState.put("trackerid", trackersID[i]);
                        listState.put("movement_status", (String) listStateMap.get("movement_status"));
                        listState.put("label", label);

                        //navixyTrackerState = getNavixyTrackerState((Map<String, Object>) objTrackerState);           // lo convierto a objeto pojo
                        respuesta.add(listState);
                    } else {

                        Map<String, Object> newResponse = new HashMap<String, Object>();
                        newResponse.put("trackerid", trackersID[i]);
                        newResponse.put("success", mapNavixyTrackerState.get("success"));
                        newResponse.put("message", mapNavixyTrackerState.get("description"));
                        newResponse.put("code", mapNavixyTrackerState.get("code"));
                        respuesta.add(newResponse);
                        //return gson.toJson(newResponse);
                    }
                }
                counter--;                                                       // obtengo el valor string del estado del candado                
                //message = gson.toJson(navixyTrackerState);

                session.getBasicRemote().sendText(gson.toJson(respuesta));

                Thread.sleep(5000);

            } while (counter > 0); // fin ciclo do while 

//----        
            System.out.println(method + " FINALIZADO");

//----    
            return message;

        } else {
            Map<String, Object> newResponse = new HashMap<String, Object>();
            newResponse.put("success", false);
            newResponse.put("message", "Unknown Method");
            newResponse.put("code", "1");
            return gson.toJson(newResponse);
        }
        //return "Metodo erroneo";
        //return null;
    }
}
